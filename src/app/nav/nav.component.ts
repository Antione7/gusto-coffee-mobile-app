import { Component, OnInit } from '@angular/core';
import * as M from 'materialize-css/dist/js/materialize.js';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
      M.AutoInit();
  }

}
