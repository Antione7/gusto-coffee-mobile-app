import { Component, OnInit } from '@angular/core';

declare let device;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  ngOnInit(){
    document.addEventListener("deviceready", function() { 
      alert(device.platform); 
      }, false); 
  }
}
