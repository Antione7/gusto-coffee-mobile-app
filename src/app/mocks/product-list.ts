import { Product } from '../models/product';

export const PRODUCT_LIST: Product[] = [
    { id: 1, name: "gaufre", price: 3, service: "restauration", quantity: null, period: null, image: "product.png" },
    { id: 2, name: "café", price: 5, service: "restauration", quantity: null, period: "hour", image: "product.png" },
    { id: 3, name: "thé", price: 5, service: "restauration", quantity: null, period: "hour", image: "product.png" },
    { id: 4, name: "clé usb", price: 5, service: "bureautique", quantity: 10, period: null, image: "product.png" },
    { id: 5, name: "location d'un serveur mutualisé", price: 5, service: "bureautique", quantity: 15, period: "month", image: "product.png" },
    { id: 6, name: "location d'un serveur dédié", price: 30, service: "bureautique", quantity: 15, period: "month", image: "product.png" },
    { id: 7, name: "croque-monsieur", price: 4, service: "restauration", quantity: null, period: null, image: "product.png" }
]