export class Product {
    id: number;
    name: string;
    price: number;
    service: string;
    quantity: number;
    period: string;
    image: string;
}
