import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from '../models/product';
import { PRODUCT_LIST } from '../mocks/product-list';



@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor() { }

  getProductList(): Observable<Product[]> {
    return of(PRODUCT_LIST);
  }
}
