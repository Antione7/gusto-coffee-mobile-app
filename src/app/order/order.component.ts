import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { OrderService } from './order.service';



@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

    productList: Product[];

    constructor(private orderService: OrderService) { }

    ngOnInit() {
        this.getProductList();
    }

    getProductList(): void {
        this.orderService.getProductList().subscribe(productList => {
            this.productList = productList;
        });
    }
}
