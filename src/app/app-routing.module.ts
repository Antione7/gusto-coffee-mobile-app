import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderComponent } from './order/order.component';


const routes: Routes = [
    { path: 'order', component: OrderComponent },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'order'
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [RouterModule]
})
export class AppRoutingModule { }